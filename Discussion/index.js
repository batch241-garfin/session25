
// JSON Objects

// It stands for JavaScript Object Notation
// It also used in other programming languages
// Core Javascript has a built in JSON object that contains methods for parsing JSON object and converting strings into Javascript
// Javascript objects are not to be confuesd with JSON
// JSON is used for serializing different data types into bytes
// Sreialization is the process of converting datainto a series of bytes for easier transmission/transfer of information
// A byte is a unit of data that is eight binary digits(1,0) that is used to represent a character(letters, numbers, or typographic symbols)

// Syntax:
// {
	// "propertA": "propertyA",
	// "propertB": "propertyB"
// }

// JSON
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// JSON - Array

// "cities": [
// 	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}
// 	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"}
// 	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}

// 	]

// PARSE: Converting JSON string into objects

let batchesJSON = '[{"batchName": "Batch X"},{"batchName": "Batch Y"}]';

console.log(JSON.parse(batchesJSON));

let stringifiedObject = '{"name": "Eric", "age": "9", "address": {"city": "Bonifacio GLobal City", "country": "Philippines"}}'
console.log(JSON.parse(stringifiedObject));

// STRINGIFY: Convert Objects into string (JSON)

// JSON.stringify

let data =
{
	name: 'Gab',
	age: 61,
	address: {
		city: "New York",
		country: "USA"
	}
}
console.log(data);
console.log(typeof data);

let stringData = JSON.stringify(data);
console.log(stringData);
console.log(typeof stringData);
